export default {
    data(){
        return {
            showNotification: false,
            errMessage: ''
        }
    },
    methods: {
        closeNotif(){
            this.showNotification = false;
            this.errMessage = '';
        }
    }
}
