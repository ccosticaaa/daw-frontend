import Vue from 'vue';
import store from '@/store';

import Router from 'vue-router';


import Login from '@/views/pages/Login';
import Register from '@/views/pages/Register';
import Welcome from '@/views/pages/Welcome';

import GuestLayout from '@/views/layouts/GuestLayout';
import UserLayout from '@/views/layouts/UserLayout';
import AdminLayout from '@/views/layouts/AdminLayout';

import TopicLayout from '@/views/layouts/TopicLayout';
import TopicListing from '@/components/topic/TopicListing';
import TopicShow from '@/components/topic/TopicShow';

import UserTopicListing from '@/components/topic/UserTopicListing';
import CustomizeSubscriptions from '@/components/topic/CustomizeSubscriptions';

import NewsLayout from '@/views/layouts/NewsLayout';
import NewsListing from '@/components/news/NewsListing';
import NewsShow from '@/components/news/NewsShow';

import UserNewsListing from '@/components/news/UserNewsListing';
import UserNewsShow from '@/components/news/UserNewsShow';


import Reports from '@/components/Reports';




Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/app',
            meta: {
                requiresAuth: true,
                User: true
            },
            component: UserLayout,
            children: [
                {
                    path: '/feed',
                    component: TopicLayout,
                    children: [
                        {
                            path: '/',
                            component: UserTopicListing,
                            name: 'app.feed',
                        },
                        {
                            path: ':topicId/news',
                            component: NewsLayout,
                            children: [
                                {
                                    path: '/',
                                    component: UserNewsListing,
                                    name: 'app.feed.news',
                                    props: (route) => {
                                        return { topicId: route.params.topicId}
                                    }
                                },
                                {
                                    path: ':newsId',
                                    component: UserNewsShow,
                                    name: 'app.feed.news.show',
                                    props: (route) => {
                                        return { newsId: route.params.newsId}
                                    }
                                }
                            ],
                        }
                    ],
                },
                {
                    path: '/customize',
                    name: 'app.customize',
                    component: CustomizeSubscriptions,
                },
            ],
        },
        {
            path: '/admin',
            meta: {
                requiresAuth: true,
                Admin: true
            },
            component: AdminLayout,
            children: [
                {
                    path: 'reports',
                    component: Reports,
                    name: 'admin.reports'
                },
                {
                    path: 'topic',
                    component: TopicLayout,
                    children: [
                        {
                            path: '/',
                            component: TopicListing,
                            name: 'admin.topic.list'
                        },
                        {
                            path: ':id',
                            component: TopicShow,
                            name: 'admin.topic.show',
                            props: (route) => {
                                return { id: route.params.id}
                            }
                        }
                    ]
                },
                {
                    path: 'news',
                    component: NewsLayout,
                    children: [
                        {
                            path: '/',
                            component: NewsListing,
                            name: 'admin.news.list'
                        },
                        {
                            path: ':id',
                            component: NewsShow,
                            name: 'admin.news.show',
                            props: (route) => {
                                return { id: route.params.id}
                            }
                        }
                    ]
                },
            ],
        },
        {
            path: '/',
            component: GuestLayout,
            children: [
                {
                    path: '/',
                    name: 'Welcome',
                    component: Welcome,
                },
                {
                    path: '/login',
                    name: 'login',
                    component: Login,
                },
                {
                    path: '/register',
                    name: 'register',
                    component: Register,
                },
            ],
        },
        {
            path: '*',
            redirect: '/'
        },

    ]
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const roleUser = to.matched.some(record => record.meta.User);
    const roleAdmin = to.matched.some(record => record.meta.Admin);
    if(requiresAuth)
    {
        let user = store.state.auth.user;
        if(!user){
            next('login');
        }
        else
        {
            if(user.role === 'Admin' && !roleAdmin)
            {
                console.log('tried to access admin route as user')
                next('login');
            }
            else
            {
                if(user.role === 'User' && !roleUser)
                {
                    console.log('tried to access user route as admin')

                    next('login');
                }
                else
                {
                    next();
                }
            }
        }
    }
    else
    {
        next();
    }
});

export default router;