import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Vuetify from 'vuetify/lib'
import router from '@/router.js';
import store from '@/store';
import ApexCharts from 'apexcharts';
import axios from 'axios';
import interceptorsSetup from '@/global/interceptors';

import Sidebar from '@/components/core/Sidebar'
import MainLayout from '@/components/core/MainLayout'

const axiosGlobalInstance = axios.create({
  baseURL: `http://localhost:4000`,
  headers: {
    'Content-Type': 'application/json'
  }
});
interceptorsSetup(axiosGlobalInstance);

Vue.component('Sidebar', Sidebar);
Vue.component('MainLayout', MainLayout);


Vue.prototype.$apexCharts = ApexCharts;

Vue.prototype.$axios = axiosGlobalInstance;

Vue.use(Vuetify, {
  theme: 'dark'
});

new Vue({
  vuetify,
  render: h => h(App),
  store,
  router

}).$mount('#app');
