import * as MUT from '@/store/mutation-types';

let state = {
    user: null
};
let mutations = {
    [MUT.UPDATE_USER](state, user) {
        state.user = user;
    },
};

let actions = {
    async updateUser({commit, dispatch}, userInfo) {
        commit(MUT.UPDATE_USER, userInfo);

    },
};

export default  {
    namespaced: true,
    state,
    actions,
    mutations
}