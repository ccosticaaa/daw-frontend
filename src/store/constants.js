// auth.js & user.js  - not yet sure how separate i want to keep these 2
export const AI_KEY_PATH = 'auto_increment_id';


export const MESSAGES_STORE_NAME = 'messages';
export const MESSAGES_KEY_PATH = 'messageId';
export const MESSAGES_OFFLINE_STORE_NAME = 'offline_messages';

export const MESSAGES_SOURCE_TYPES = {
    LOCAL: 'LOCAL',
    EXTERNAL : 'EXTERNAL'
}


export const USER_OFFLINE_STORE_NAME = 'user';
export const USER_KEY_PATH = '_id';


export const GROUP_STORE_NAME = 'group_info';
export const GROUP_KEY_PATH = 'groupId';


export const FRIENDS_INFO_STORE_NAME = 'friends';
export const FRIENDS_KEY_PATH = '_id';