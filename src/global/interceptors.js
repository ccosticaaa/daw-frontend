import store from '@/store';

export default function setup(axiosGlobalInstance) {
    axiosGlobalInstance.interceptors.request.use(async function (config) {
            const loggedInUser = store.state.auth.user;
            if(loggedInUser)
            {
                config.headers['Authorization'] = `Bearer ${loggedInUser.token}`;
            }

            return config;
        },
        function (err) {
            console.log('here ?');
            return Promise.reject(err);
        });
}