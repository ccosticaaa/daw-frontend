let config = {
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src/"),
            "ROOT": path.resolve(__dirname),
        },
        aliasFields: ["@", "ROOT"]
    }
};

module.exports = config;
